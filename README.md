### このプロジェクトの目的
SAST semgrepのリモートカスタムルールの挙動を検証することを目的とするプロジェクトです。

## 利用方法
Javaのコード内に下記の脆弱な記載をあらたなファイルに追加してコミットします。

``` java 
	public static void listFiles(String dir) throws Exception {
		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec(new String[] {"sh", "-c", "ls " + dir});
		int result = proc.waitFor();
		if (result != 0) {
			System.out.println("process error: " + result);
		}
		InputStream in = (result == 0) ? proc.getInputStream() :
										proc.getErrorStream();
		int c;
		while ((c = in.read()) != -1) {
			System.out.print((char) c);
		}
	}
```

この時、 ```.gitlab-ci.yml```が外部のルールセット```gitlab.com/naosasaki-demo/study/security/sast-customize-rule```を参照している場合は、上記の検出は抑止されます。ルールセットの参照をコメントアウトすると、新たに検出されます。これにより外部のルールセットを適切に読み込んでいることを確認できます。

